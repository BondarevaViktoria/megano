from django.contrib.auth import login
from django.contrib.auth.password_validation import validate_password

from rest_framework.exceptions import ValidationError
from rest_framework.generics import UpdateAPIView
from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response

import json

from .serializers import UserRegisterSerializer, LoginSerializer, ChangePasswordSerializer, ProfileUpdateSerializer, \
    AvatarUpdateSerializer


class UserLoginAPIView(APIView):
    """
    Класс для входа пользователя в систему
    """
    def post(self, request: Request):
        body = json.loads(request.body)
        serializer = LoginSerializer(data=body)
        if serializer.is_valid():
            user = serializer.validated_data['user']
            login(request, user)
            return Response(status=200)
        else:
            return Response(data=serializer.errors, status=400)


class UserRegisterAPIView(APIView):
    """
    Класс описывающий регистрацию пользователя на сайте
    """
    def post(self, request: Request):
        body = json.loads(request.body)
        serializer = UserRegisterSerializer(data=body)
        if serializer.is_valid():
            user = serializer.save()
            login(request, user)
            return Response(status=200)
        else:
            return Response(data=serializer.errors, status=400)


class ProfileUpdateAPIView(APIView):
    """
    Класс описывающий внесение изменений в данные профиля пользователя
    """

    def post(self, request):
        user = request.user

        data = {
            'fullName': request.data.get('fullName'),
            'email': request.data.get('email'),
            'phone': request.data.get('phone')
        }

        serializer = ProfileUpdateSerializer(data=data)
        if serializer.is_valid():
            serializer.update(user, data)
            return Response(serializer.data, status=200)
        else:
            return Response(serializer.errors, status=400)

    def get(self, request: Request):
        if request.user.is_authenticated:
            user = request.user
            data = {
                'fullName': user.profile.full_name,
                'email': user.email,
                'phone': user.profile.phone_number,
                'avatar': {
                    'src': user.profile.avatar.url,
                    'alt': user.profile.avatar.name,
                },
            }
            return Response(data=data, status=200)
        return Response(status=400)


class ChangePasswordAPIView(UpdateAPIView):
    """
    Класс описывающий изменение пароля пользователя
    """

    def post(self, request: Request):
        user = request.user
        if user.is_authenticated:
            data = request.data
            new_password1 = data.get('newPassword')
            serializer = ChangePasswordSerializer
            if serializer.is_valid:
                try:
                    validate_password(new_password1)
                    user.set_password(new_password1)
                    user.save()
                    login(request, user)
                    return Response(status=200)
                except ValidationError:
                    print('Non valid')
        return Response(status=400)


class ChangeAvatarAPIView(APIView):
    """
    Класс описывающий изменение аватарки в профиле пользователя
    """
    def post(self, request: Request):
        user = request.user

        data = {
            'src': request.data.get('src'),
            'alt': request.data.get('alt')
        }

        serializer = AvatarUpdateSerializer
        if serializer.is_valid():
            serializer.update(user, data)
            return Response(serializer.data, status=200)
        else:
            return Response(serializer.errors, status=400)

