from django.contrib import admin
from .models import (
    Category,
    Images,
    Specifications,
    Subcategory,
    Tag,
    Review,
    Product, SaleProduct,
)


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = 'pk', 'name'
    list_display_links = 'pk', 'name'


class ImagesInline(admin.TabularInline):
    model = Images


class ReviewsInline(admin.StackedInline):
    model = Review


class SpecificationsInline(admin.TabularInline):
    model = Specifications


class SalesInline(admin.TabularInline):
    model = SaleProduct


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = (
        'pk',
        'title',
        'count',
        'price',
        'freeDelivery',
        'date',
        'category',
    )
    list_display_links = 'pk', 'title'
    inlines = [
        SalesInline,
        ImagesInline,
        SpecificationsInline,
        ReviewsInline,
    ]

    def get_queryset(self, request):
        return Product.objects.all().select_related('category')


class SubcategoriesInline(admin.TabularInline):
    model = Subcategory


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = 'pk', 'title'
    inlines = [
        SubcategoriesInline,
    ]

