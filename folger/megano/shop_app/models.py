from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


def upload_product_images_path(instance: 'Images', filename: str) -> str:
    return 'products/product_{id}/{filename}'.format(
        id=instance.product.id,
        filename=filename,
    )


def upload_subcategory_photo_path(instance: 'Subcategory', filename: str) -> str:
    return 'categories/subcategories/{filename}'.format(filename=filename)


class Subcategory(models.Model):
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to=upload_subcategory_photo_path)
    category = models.ForeignKey(
        'Category', related_name='subcategories', on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = 'subcategory'
        verbose_name_plural = 'subcategories'

    def __str__(self):
        return self.title


def upload_category_photo_path(instance: 'Category', filename: str) -> str:
    return 'categories/categories/{filename}'.format(filename=filename)


class Category(models.Model):
    title = models.CharField(max_length=100, db_index=True)
    image = models.ImageField(upload_to=upload_subcategory_photo_path)

    class Meta:
        verbose_name = 'category'
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.title


class Tag(models.Model):
    name = models.CharField(max_length=50)
    category = models.ForeignKey(Subcategory, on_delete=models.CASCADE, related_name='tags')

    class Meta:
        verbose_name = 'tag'
        verbose_name_plural = 'tags'

    def __str__(self):
        return self.name


class Review(models.Model):
    author = models.CharField(max_length=100)
    email = models.EmailField()
    text = models.TextField(max_length=1000, blank=True)
    rate = models.SmallIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(5)]
    )
    date = models.DateTimeField(auto_now_add=True)
    product = models.ForeignKey(
        'Product', on_delete=models.CASCADE, related_name='reviews'
    )

    class Meta:
        verbose_name = 'review'
        verbose_name_plural = 'reviews'

    def __str__(self):
        return self.author


class Specifications(models.Model):
    product = models.ForeignKey(
        'Product', related_name='specifications', on_delete=models.CASCADE
    )
    name = models.CharField(max_length=20)
    value = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'specification'
        verbose_name_plural = 'specifications'

    def __str__(self):
        return self.name


def product_image_directory_path(instance: 'Product', filename: str) -> str:
    return 'uploads/products/product_{pk}/{filename}'.format(
        pk=instance.pk,
        filename=filename,
    )


class SaleProduct(models.Model):
    product = models.OneToOneField('Product', on_delete=models.CASCADE, related_name='sale')
    new_price = models.DecimalField(decimal_places=2, max_digits=100)
    date_from = models.DateTimeField(auto_now_add=True)
    date_to = models.DateTimeField()


class Product(models.Model):
    title = models.CharField(max_length=100, blank=True)
    description = models.TextField(max_length=500, blank=True)
    fullDescription = models.TextField(blank=True)
    count = models.IntegerField()
    price = models.DecimalField(decimal_places=2, max_digits=100)
    freeDelivery = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)
    category = models.ForeignKey(Subcategory, on_delete=models.SET_NULL, null=True)
    tags = models.ManyToManyField(Tag, related_name='products')
    is_available = models.BooleanField(default=False)
    is_limited = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'product'
        verbose_name_plural = 'products'

    def __str__(self):
        return self.title

    def __setattr__(self, key, value):
        super().__setattr__(key, value)
        if key == 'count' and self.count == 0:
            self.is_available = False

    def current_price(self):
        try:
            price = self.sale.new_price
            return price
        except SaleProduct.DoesNotExist:
            return self.price


class Images(models.Model):
    product = models.ForeignKey(
        Product, related_name='images', on_delete=models.CASCADE
    )
    image = models.ImageField(upload_to=upload_product_images_path)

    class Meta:
        verbose_name = 'image'
        verbose_name_plural = 'images'



