from rest_framework.generics import GenericAPIView
from rest_framework.mixins import ListModelMixin

from shop_app.models import Category, Product, Tag
from shop_app.serializers import CategorySerializer, ProductCatalogSerializer, TagSerializer, SaleSerializer, \
    ProductSerializer


class CategoryAPIView(ListModelMixin, GenericAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def get(self, request):
        response = self.list(request)
        return response


class CatalogAPIView(ListModelMixin, GenericAPIView):
    queryset = (
        Product.objects.all()
        .prefetch_related('images')
        .prefetch_related('tags')
        .prefetch_related('reviews')
        .select_related('sale')
    )
    serializer_class = ProductCatalogSerializer

    def get(self, request):
        return self.list(request)


class SalesAPIView(ListModelMixin, GenericAPIView):
    queryset = Product.objects.filter(sale__isnull=False).select_related('sale')
    serializer_class = SaleSerializer

    def get(self, request):
        return self.list(request)


class TagsAPIView(ListModelMixin, GenericAPIView):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer

    def get(self, request):
        return self.list(request)


class BannersAPIView(ListModelMixin, GenericAPIView):
    queryset = Product.objects.filter(is_available=True).select_related('sale')
    serializer_class = ProductCatalogSerializer

    def get(self, request):
        return self.list(request)


class LimitedProductsAPIView(ListModelMixin, GenericAPIView):
    serializer_class = ProductSerializer
    queryset = Product.objects.filter(is_limited=True, is_available=True)[:16]

    def get(self, request):
        return self.list(request)


class ProductsPopularAPIView(ListModelMixin, GenericAPIView):
    queryset = Product.objects.all().select_related('sale')
    serializer_class = ProductCatalogSerializer

    def get(self, request):
        return self.list(request)
